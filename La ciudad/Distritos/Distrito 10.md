## Codigo de distrito: 10
Un distrito empresarial, en el cual se mueve el dinero y se ocupan de la burocracia, las calles estan limpias y los policias se pueden ver con facilidad. En este distrito se pueden encontrar tiendas de lujos facilmente y a pesar de lo gris de la calle, la gente se mueve por ella con grandes sonrisas y el tranvia siempre va lleno hacia cualquier parte.
## Lugares de interes

### Teatro de los hermanos Rufino
Es un gran teatro con colores muy vivos, en una de las zonas mas concurridas del distrito 10 y siempre estan preparando alguna funcion. Es un lugar social en el que despues de cada funcion nueva la gente se reune a charlar. Se pueden ver grandes engranajes que alzan y bajan el telón enorme que hace que sea uno de los teatros con mejor fama del distrito.
En los palcos se pueden ver los diferentes colores de casa, que usan cuando vienen a ver una obra.

Teatro fue montado por un horario que decidio dejar la vida en el distrito 12 por su amor por Giuseppe, dado que esta mal visto para su casa.

#### PNJ
- Matteo Inmediche
- Giuseppe "Estrella"
- Directora Ginevra Albitche

### Taller de Leonardo
Es el lugar en el que tranquilamente salvo algunas situaciones en las que algo explota en las que Emma Leonardo se ocupa de generar sus invenciones.

#### PNJ
- Emma Leonardo
- Valentina "Ama de llaves"