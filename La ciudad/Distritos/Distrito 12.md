## Codigo de distrito: 12
{Descripcion}
### Lugares de interes

### Palacio de Cristal.
Es un gran palacio erguido de cristal y acero que muestra la elegancia de la clase alta, con dos grandes puertas metalicas con grandes engranajes para abrirlas.
Esta resguardado por la gran guardia horaria.

Cada jueves se reunen los representantantes de las familias horarias.

### Gran parque del libertador.
Es un parque enorme en el centro de la zona horaria, en el cual se pueden ver multitud de tipos de Arboles, con un gran lago artificial central y de dia se pueden ver a numerosas parejas en los botes impulsados por pedales en pleno cortejo.

PNJ:
- Dedalo el encargado de las barcas. Un hombre de avanzada edad con un gran gusto por la tecnologia a pesar de ser de clase baja.
- Enrique Vicenzo el Veleta, encargado de la casa de comidas y uno de los chef de comida rapida mejor pagados, Bien afeitado y con una sonrisa enorme, es un minutario que disfruta de la comida.
- Jackie 'Scrapy' Rusty, es un joven de apenas 12 años que perdio al padre y ayuda a su madre vendiendo el periodico entre la clase alta. Tambien se ocupa de entregar los panfletos de la señora Blackmail.

Cada dia se pueden ver obras de teatro y pequeños conciertos en los gacebos, siendo muy comun para los niños teatros de marionetas automatizados.

### Gentes
#### La señorita Blackmail.
- Realmente la señorita Blackmail es un grupo de trabajadores, condellas, amas de llaves que quieren sacar un sobresueldo y por ello saben mas de lo normal de sus señores.