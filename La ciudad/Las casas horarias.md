## Du'Herrizabetta
Colores: Granate, Verde Mar
Escudo: Leon verde sobre fondo granate
Persona al cargo: Tobias Du'Herrizabetta
Pequeña historia: Es una familia proveniente de un largo linage de colonizadores, los cuales consiguieron el rango noble mediante su capacidad de conversión. Tienen diferentes colonias alrededor del mundo y se ocupan de surtir de bienes extraños a la ciudad de los mil engranajes.

## Wallfellstrudell
Colores: Azul y Dorado
Escudo: Dragon Rojo sobre fondo dorado
Persona al cargo: Baldric Walfellstrudell
Pequeña historia: Es una de las casas mas antiguas, se remonta al hermano del primer rey, anteriormente consejeros ansiando el poder en el reino, en cuanto fueron repudiados quisieron governar de otra forma. Son pirañas politicas capaz de destripar a cualquiera con tal de tener un trato de fabor.

## Wazallbelthias
Colores: Marron y el Verde
Escudo: Oso Verde en fondo marron.
Persona al cargo: Gunther Wazallbelthias
Pequeña historia: Era una familia noble menor pero disponia de las tierras mas abundantes en recursos naturales, Hierro, zinc... Actualmente tienen la potestad de las minas y los desarrollos armamentisticos.

## Heshro
Colores: Blanco y Rojo
Escudo: Rayo rojo en fondo blanco.
Persona al cargo: Leonardo Heshro
Pequeña historia: Era una familia interior con tierras centrales que nunca necesito un ejercito propio, hoy en dia son mecenas del arte y ciencia. Tienen una capacidad increible para encontrar a genios y adueñarse de sus capacidades.

## Dambrah
Colores: Verde y Rojo
Escudo: Venado Rojo en fondo verde
Persona al cargo: Damian Dambrah
Pequeña historia: Es una familia noble bastante orientada a la caza, que queria mejores formas de cazar, ya cansados de la caza del zorro anual. Ahora mismo por sus positicas se les permite cazar a los reos, forman cacerias mensuales y se consideran los ejecutores.

## Misgeaga
Colores: Dorado y Negro
Escudo: No
Persona al cargo: Charlie Misgeaga
Pequeña historia: Familia pudiente de mercaderes, que tiene una gran red de influencia y de tratado de mercancias "especiales". Fueron vitales para la mejora de la medicina y tienen numeroros boticarios por la ciudad.

## Amon'Guirashi
Colores: Dorado y verde
Escudo: No
Persona al cargo: Rhys Amon'Guirashi
Pequeña historia: Fueron mercaderes de exclavos especializados en alta sociedad, ya que tenian escuelas de buenos modales para ellos. Esas escuelas se reconvirtieron para el servicio.

## Inmediche
Colores: Dorado y rojo
Escudo: No
Persona al cargo: Mary Inmediche
Pequeña historia: Familia pudiente que se especializo en la medicina y consiguio mucho dinero gracias a sus remedios.

## Wolfhound
Colores: Dorado y plateado
Escudo: No
Persona al cargo: Victoria Wolfhound
Pequeña historia: Casa que se hizo a si misma creando armas de fuego.

## D'Monde
Colores: Dorado y Aguamarina
Escudo: No
Persona al cargo: Chevelier D'Monde
Pequeña historia: Consiguieron su gran fortuna gracias a la venta de obras de arte.

## D'Linchestain
Colores: Dorado y Naranja
Escudo: No
Persona al cargo: Adelaida D'Lichestain
Pequeña historia: Antiguo grupo de mercenarios.

## Primullonsa
Colores: Dorado y el morado loto
Escudo: No
Persona al cargo: Jessica Primullonsa
Pequeña historia: Los mejores asesinos a sueldo.