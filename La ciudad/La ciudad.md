# Breve información:
Todas las fechas se miden a partir de la fecha en la que se fundo la ciudad conocida como CE Creación del Engranaje. Siendo el año 0 el año en el que se forjo la alianza de las 12 casas.

# Historia


# Las 12 casas:
Para crear la ciudad hacían falta tierras y dinero, por eso se reunieron los que en un inicio fueron conocidos como la conjura de los 12, los nobles pusieron tierras y los burgueses el dinero, todo pagado al visionario. El poder en la ciudad lo tendría una persona por cada casa, persona que llevaría su casa y tendría al cargo a casas menores conocidas como banderizas.

## Como mantener una casa:
Todo líder de una casa tiene que responder ante las 11 familias restantes y tiene que cumplir una serie de requisitos o se le puede entregar el mando de la casa a una banderiza.
- Tiene que poder mantener el legado de la casa (Tiene que poder tener hijos)
- Esta obligado a contribuir a la ciudad llevando negocios y manteniéndose atento a ellos.
- Tiene que tener una conducta intachable dentro y fuera de casa.

Un simple rumor puede llegar a destruir el trono de una casa horaria y hacer que se alce una banderiza, entre los Horarios son comunes las traiciones y los golpes bajos. La política es dura pero mas duro es mantener el trono.

## Diferencias entre casas:
Aunque todos tienen que cumplir sus requisitos hay diferencias entre las casas que vienen de la nobleza y aquellas que aportaron dinero. Pues como las casas que aportaron tierras siguen manteniendo el titulo de ellas a pesar de que salgan del consejo, lo normal es que se les trate de manera diferente y entre ellos existe un elitismo. Cuando una casa con escudo cae esta tiene que entregarle el mando del consejo a alguien que venga de su mismo árbol genealógico. Por que el apellido sigue manteniendo las tierras en la ciudad.

## Los distritos

### 01-08 - Distritos Secondos
Distritos llenos de fabricas, muelles y otras estructuras para trabajar, las casas son de pisos multifamiliares y estan hacinados.

### 09-11 - Distritos Minutarios
Distritos de gobierno, con policia, bancos y demas estamentos burocraticos. Tambien tienen las tiendas importantes

### 12 - Distrito Horario
Distrito limpio con grandes mansiones y lugares de recreo para los horarios.