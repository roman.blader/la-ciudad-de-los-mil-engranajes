## La gran mansion Du'Herrizabetta
Es una mansion imponennte con unos grandes jardines y piscina de color verdemar, Esta pintada de color granate con el acero de las puertas y balcones de color verdermar.
Con una gran galeria de cristal desde la que se puede ver todo el jardin y admirarlo. La puerta esta coronada de una vidrira en la que se puede ver un leon verde sobre fondo granate.
En la parte mas cercana a la entrada varios sauces ocultan la entrada y rosales se pueden ver en el camino a la entrada principal.

## Zonas importantes:
### Recibidor:
Al entrar por el gran porton se pueden ver unas inmensas escaleras que llevan a la sala de eventos engalanadas con una gran alfombra granate con el exterior dorado y unos pasamanos de acero forjado con relieves de Leones en plena caza.
En la entrada siempre se puede ver al Mayordomo esperando en la puerta para anunciar a los invitados.

### Salon de eventos.
Es una amplia habitacion con numerosas mesas engalanadas con manteleria blanca pulcra y una zona elevada en la que habitualmente dos Arpas tocan algun tipo de musica. Tiene un acceso a la cocina por medio de elevadores. Da acceso al jardin por la zona norte y a la gran galeria de cristal por la zona oeste.

### Los Jardines.
Son unos inmensos jardines, donde habitualmente hay una docena de perros de caza jugando y corriendo por ella, con un gacebo enorm con un sistema de engranajes para que valla cambiando de color con la cristaleria donde suele haber comida y se refugia la gente cuando llueve.
El jardin esta muy bien cuidado por el Jardinero y su aprendiz Dought.

### La Caballeriza/cochera.
En la parte este de la mansion esta el gran establo, es enorme, de color blanco con los caballos y una nueva cochera construida en los ultimos años. Se encarga de los caballos el mozo de establo Tobias. Para la parte de la cochera automatizada esta Sara la mecanica.

## Las Cocinas.
Nada mas entrar se puede ver una zona enorme en la que hay numerosos hornos y fogones automatizados que brillantemente Elisa "la yaya" usa para crear grandes platos. Cuando hay eventos se contrata servicio a mayores para que ayude a Elisa en la cocina.
Tambien hay una gran puerta que esta helada donde se creo una camara frigorifica que funciona gracias a un invento de Hernan.

## Las zona de servicio.
Es una zona apartada con varias mesas y juegos de tablero y cartas, donde el servicio de la casa y de los invitados pasa el tiempo libre. Tiene varias mesas de poker y una zona ventilada donde fumar.

Zona Extra:
- Habitacion de Lupita/Antonio Hernan
- Habitacion de Felipe Hernan
- Habitacion de Dought (Compartida)
- Habitacion de Alber Du'Herrizabetta
- Habitacion de Franchesca/Paolo Du'Herrizabetta