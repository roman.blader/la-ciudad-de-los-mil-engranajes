## Nota:
Este personaje no tiene importacia el sexo o genero y por ello se dan dos opciones.
## Nombre:
Elejir entre: Lupita o Antonio
Apellido: Hernan Salcedo.
## Descripcion:
Lo que mas marca este personaje son unos ojos Verdes intensos y una piel tersa y suave de un color oliva. Toda su belleza es natural.
Adora a su Padre y haria cualquier cosa para cuidarlo, dado que considera que su padre no soportaria su perdida, despues de la de la madre. Sabe que el padre solo consiguio el trabajo por que su contratador desea tener una relación.
### Lupita
Vestido rojo hasta los tobillos y unos zapatos con un tacon lijero de hebilla que le alzan el cuerpo.
En caso de que se le llegue a ver con un traje de gala sera rojo con bolantes y bordados de rosas amarillas. Los zapatos seran negros de tacon y llevara unas medias negras.

### Antonio
Camisa blanca sedosa con la parte de arriba algo abierta, sin barba en la cara ni apenas pelo en el pecho, unos pantalones marrones con un cinturon negro y unas botas marrones.
En caso de que este en la fiesta o lo vean antes, llevara un traje negro con la camisa y sin corbata o pajarita y unos zapatos de calidad.

## Objetivos personales:
- Adora a su padre y quiere que mantenga su empleo.
- No soporta la jaula de oro en la que vive y quiere la libertad.
- Llegar a enamorarse alguna vez.
- Ver mundo.